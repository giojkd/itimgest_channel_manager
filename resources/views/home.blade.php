<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <title>Itimgest - Channel Manager</title>
</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-md-12 mt-4 mb-4 text-center">
        <img class="d-inline" src="https://www.itimgest.com/img/LOGO_ITIMGEST_2019_200h.png" alt="">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <h1 class="display-4">Ciao!</h1>
          <p class="lead">Qui potrai trovare i file di scambio con i portali</p>
          <hr class="my-4">
          <p>Cerca i file di scambio che ti interessano!</p>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <h2>Immobiliare.it</h2>
      <table class="table table-hover table-striped">
        <thead>
          <tr>
            <th>Agenzia</th>
            <th>Collegamento</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Tutte</td>
            <td> <a target="_blank" href="/feed/immobiliare">Link</a> </td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h2>Casa.it</h2>
      <table class="table table-hover table-striped">
        <thead>
          <tr>
            <th>Agenzia</th>
            <th>Collegamento</th>
          </tr>
        </thead>
        <tbody>
          @foreach($agencies as $agency)
          <tr>

            <td>{{$agency->agency_name}}</td>
            <td> <a target="_blank" href="/feed/casa/?agency_id={{$agency->agency_id}}">Link</a> </td>

          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
