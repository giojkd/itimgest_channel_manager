<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Estate extends Model
{
  protected $table = 'estate';
  protected $primaryKey = 'estate_id';

  public function category(){
    return $this->belongsTo('App\Category','IDCategoria','Categoria');
  }
  public function type(){
    return $this->belongsTo('App\Type','IDTipologia','IDTipologia');
  }

  public function agency(){
    return $this->belongsTo('App\Agency','agency_id','agency_id');
  }

  public function description(){
    return $this->hasMany('App\EstateText','estate_id','estate_id')->where('txt_type','estate_description');
  }

  public function rooms(){
    #DB::connection()->enableQueryLog();
    $rooms = DB::table('estate_rooms as er')
    ->leftJoin('immobiliareit_values as iv','iv.value_id','=','er.room_type_id')
    ->where('iv.value_type','TipoVano')
    ->where('er.estate_id',$this->estate_id)
    ->get();
    $return = [];
    if(!is_null($rooms)){
      foreach($rooms as $room){
        $return[strtolower(str_replace(' ', '-', trim($room->value)))] = ($room->quantity>0) ? $room->quantity : 0;
      }
    }
    return $return;
    #$queries = DB::getQueryLog();
    #dd($queries);
  }

  public function photos(){
    $photos =
    $this->hasMany('App\EstatePhoto','estate_id','estate_id')
    ->leftJoin('files','files.file_id','=','estate_photos.file_id')
    ->where('blueprint','0')
    ->orderBy('sort_order');
    return $photos;
  }

  public function binaryFields(){
    $fields = DB::table('estate_binary_fields as ebs')
    ->select('iv.value as name','ebs.value as status')
    ->leftJoin('immobiliareit_values as iv','iv.value_id','=','ebs.field_id')
    ->where('iv.value_type','CampoBinario')
    ->where('ebs.estate_id',$this->estate_id)
    ->get();
    $return = [];
    if(!is_null($fields)){
      foreach($fields as $field){
        $return[strtolower(str_replace(' ', '-', trim($field->name)))] = ($field->status == 1) ? 'Y' : 'N';
      }
    }

    return $return;
  }

  public function value($type){

    DB::connection()->enableQueryLog();
    $return = DB::table('immobiliareit_values')
    ->where('value_type','LIKE',"'".$type."'")
    ->where('value_id',(array)$this[$type])
    ->first();
    if(!is_null($return)){
        return $return;
    }else{
      $obj = new \stdClass();
      $obj -> value = '';
      return $obj;
    }
    $queries = DB::getQueryLog();
    dd($queries);
    exit;
  }
}
