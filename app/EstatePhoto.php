<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstatePhoto extends Model
{
    protected $table = 'estate_photos';
}
