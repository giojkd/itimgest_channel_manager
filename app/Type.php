<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
  protected $table = 'immobiliareit_categoria';
  protected $primaryKey = 'IDTipologia'; 
}
