<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\ArrayToXml\ArrayToXml;
use App\CasaCity;

class EstateController extends Controller
{

  public function home(){

    $agencies = \App\Agency::where('agency_status',1)->orderBy('agency_name')->get();
    $data = ['agencies'=>$agencies];
    return view('home', $data);
  }

  public function feedCasa(Request $request){

    $data = $request->all();


    $valuesMap = [
      'arredamento' => [
        '' => 1,#Non arredato
        '' => 2,#Parzialmente arredato
        '' => 3,#Completamente arredato
      ],
      'condizioniImmobile' => [
        '' => -1, #Non indicato
        '' => 1, #Abitabile
        '' => 2, #Nuovo
        '' => 3, #Ristrutturato
        '' => 4, #Da Ristrutturare
        '' => 5, #In costruzione
      ],
      'riscaldamento' => [
        '' => -1, #Non indicato
        '' => 1, #Nessuno
        '' => 2, #Autonomo
        '' => 3, #Centralizzato
        '' => 4, #Centralizzato con contabilizzazione individuale
      ],
      'tipo' => [
        '' => 1,
      ],
      'statoImmobile'=>[
        '' => -1, #Non indicato
        '' => 1, #Occupato
        '' => 2, #Libero
        '' => 3, #Nuda Proprietà
        '' => 4, #Affittato
      ],
      'buildingType'=>[
        '' => 1, #Residenziale
        '' => 2, #Vacanze
        '' => 3, #Commerciale
      ]
    ];
    $setLang = 'it';
    $agency_id = $data['agency_id'];
    $agency = \App\Agency::find($agency_id);
    $estates = \App\Estate::with(['type','agency','description','photos'])->take(20)->where('agency_id',$agency_id)->where('status',0)->where('parent_estate_id',0)->get();


    $roombed = 0;
    $roombed += (isset($rooms['camera-matrimoniale'])) ? $rooms['camera-matrimoniale'] : 0;
    $roombed += (isset($rooms['camera-singola'])) ? $rooms['camera-singola'] : 0;
    foreach($estates as $estate){

      $photos = $estate->photos;


      if(!is_null($photos)){
        foreach($photos as $photo){
          $_aux = json_decode($photo->formats,1);
          $images[] = [
            '_attributes' => ['type'=>'Image'],
            '_value' => $_aux['x2560']
          ];
        }
      }

      $estateDescriptions = '';
      foreach($estate->description as $description){
        if($setLang == $description->lang){
          $estateDescriptions = strip_tags($description->value);
        }
      }


      $condizioniImmobile = $estate->value('CondizioniImmobile');
      $tipoCostruzione = $estate->value('TipoCostruzione');
      $classeEnergetica = $estate->value('ClasseEnergetica');
      $ipeUnitaMisura = $estate->value('IPEUnitaMisura');
      $riscaldamento = $estate->value('Riscaldamento');
      $arredamento = $estate->value('Arredamento');
      $statoImmobile = $estate->value('StatoImmobile');

      $rooms = $estate->rooms();



      $casaCity = \App\CasaCity::where('name','Impruneta')->where('province','Firenze')->where('region','Toscana')->first();
      $casaZone = \App\CasaZone::where('istat_city',$estate->ISTAT)->first();
      $listing[] = [
        'action' => 0,
        'agencycode' => $agency->agency_vat_number,
        'reference' => ['c_data'=>$estate->ref],
        'listingid' => $estate->estate_id,
        'contracttype' => ($estate->Contratto == 'V') ? '2' : '1',
        'isauction' => false,
        'roombed' =>$roombed,
        'furnished' => $valuesMap['arredamento'][$arredamento->value], #<-------- check better
        'condition' => (isset($valuesMap['condizioniImmobile'][$condizioniImmobile->value])) ? $valuesMap['condizioniImmobile'][$condizioniImmobile->value] : '',
        'hasbalcony' => (isset($rooms['balcony']) && $rooms['balcone'] > 0) ? true : false,
        'hasterrace' => (isset($rooms['terrazza']) && $rooms['terrazza'] > 0) ? true : false,
        'heatingtype' => (isset($valuesMap['riscaldamento'][$riscaldamento->value])) ? $valuesMap['riscaldamento'][$riscaldamento->value] : '',
        'housetypology' => (isset($valuesMap['tipo'][$estate->type->NomeTipologia])) ? $valuesMap['tipo'][$estate->type->NomeTipologia] : '',
        'bathrooms' => (isset($rooms['servizio']) && $rooms['servizio'] > 0) ? $rooms['servizio'] : 0,
        'floor' => $estate->piano,
        'rooms' => $estate->NrLocali,
        'occupationstate' => (isset($valuesMap['statoImmobile'][$statoImmobile->value])) ? $valuesMap['statoImmobile'][$statoImmobile->value] : '',
        'realestatetype' => (isset($valuesMap['buildingType'][$estate->type->Categoria])) ? $valuesMap['buildingType'][$estate->type->Categoria] : '',
        'size' => (string)$estate->MQSuperficie,
        'address' => [
          '_attributes' => ['isaddressvisibleonsite' => true],
          'city' => (isset($casaCity)) ? $casaCity->id : '',
          'zone' => (isset($casaZone)) ? $casaZone->id : '',
          'street' => [
            '_cdata' => $estate->route
          ],
          'number' => [
            '_cdata' => $estate->street_number
          ],
          'zip' => $estate->postal_code
        ],
        'map' => [
          'latitude' => $estate->lat,
          'longitude' => $estate->lng,
          'mapzoom' => 3,
          'latitudemapcenter' => $estate->lat,
          'longitudemapcenter' => $estate->lng,
          'ismapvisible' => true,
          'isrealestatevisibleonmap' => true
        ],
        'description' => [
          'italian' => [
            '_cdata' => $estateDescriptions
          ]
        ],
        'building' => $estate->SpeseMensili,
        'price' => [
          'purchaseoption' => false,
          'value' => (string)$estate->prezzo,
          'privatenegotiation' => $estate->TrattativaRiservata,
          'max' => (string)$estate->prezzo,
          'min' => (string)$estate->prezzo
        ],
        'box' => [
          'size' => 0,
          'type' => -1
        ],
        'parkingspace' => [
          'number' => (isset($rooms['garage'])) ? $rooms['garage'] : 0,
          'type' => 0
        ],
        'garden' => [
          'size' => 0,
          'type' => (isset($rooms['giardino'] ) && $rooms['giardino'] >0) ? 1 : 0
        ],
        'media'=>['images'=>['image'=>isset($images) ? $images : []]],
        'energyefficiency'=>[
          'energyefficiencyratingid' => (isset($valuesMap[$classeEnergetica->value])) ? $valuesMap[$classeEnergetica->value] : '',
          'energyefficiencyratingvalue' => $estate->ClasseEnergeticaTxt,
          'energyefficiencyvaluenonrenewable' => 0.00,
          'energyefficiencyvaluerenewable' => 0.00,
          'energyefficiencyvaluewinter' => 0.00,
          'energyefficiencyvaluesummer' => 0.00,
          'iscubemeters' => false,
          'epe' => 0,
          'epi' => 0,
          'isquitezeroenergyestate' => false
        ],
        'features' => [
          'conditionedair' => (isset($binaryFields['aria-condizionata']) && $binaryFields['aria-condizionata'] == 'Y') ? 1 : 0,
          'swimmingpool' => (isset($binaryFields['piscina']) && $binaryFields['piscina'] == 'Y') ? 1 : 0,
          'attic' => false, #<------ check better,
          'cellar' => false, #<------ check better,
          'concierge' => false, #<------ check better,
        ]

      ];
    }
    ;


    $return = [
      'container'=>[
        '_attributes'=>[
          'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
          'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema',
          'xsi:nonamespaceschemalocation' => 'https://feed.casa.it/v2/xsd/XmlRealEstate.xsd'
        ],
        'listings' =>[
          '_attributes' =>['version'=>'2.0.0'],
          'listing' => $listing
        ]
      ]
    ];


    $result =  ArrayToXml::convert($return,[

      'rootElementName' => 'xml',
      '_attributes' => [
        'version' => '1.0',
        'encoding' => 'UTF-8',
      ],

    ]);
    header('Content-Type', 'text/xml');
    header('charset','utf-8');
    echo $result;

  }

  public function feedImmobiliare(){

    $setLang = 'it';
    $estates = \App\Estate::with(['type','agency','description','photos'])->where('status',0)->where('parent_estate_id',0)->take(10)->get();

    $return = [
      'version' => 2.7,
      'metadata' => [
        'publisher' => [
          'name' => 'Italiana Immobiliare SPA',
          'site' => 'https://www.italianaimmobiliare.it',
          'email' => 'info@italianaimmobiliare.it',
          'phone' => '++39055361146'
        ],
        'build-date' => date('Y-m-d').'T'.date('H:i:s'),
        'multipage' => [
          'current' => 1,
          'last' => 1
        ]
      ]
    ];
    foreach($estates as $estate){

      if(isset($estate->type)){
        $buildingMap = implode('/',[$estate->type->Categoria,$estate->type->NomeTipologia,$estate->type->NomeTipologia]);
      }else{
        continue;
      }

      $condizioniImmobile = $estate->value('CondizioniImmobile');
      $tipoCostruzione = $estate->value('TipoCostruzione');
      $classeEnergetica = $estate->value('ClasseEnergetica');
      $ipeUnitaMisura = $estate->value('IPEUnitaMisura');
      $riscaldamento = $estate->value('Riscaldamento');

      $rooms = $estate->rooms();
      $binaryFields = $estate->binaryFields();
      $pictures = [];
      $photos = $estate->photos;
      #dd($photos);

      if(!is_null($photos)){
        foreach($photos as $photo){
          $_aux = json_decode($photo->formats,1);
          $pictures[] = [
            '_attributes' => ['position'=>$photo->sort_order],
            '_value' => $_aux['x2560']
          ];
        }
      }

      $estateDescriptions = [];
      $extraFeatures = [];
      foreach($estate->description as $description){
        if($setLang == $description->lang){
          $estateDescriptions[] =
          [
            'description'=>[
              '_attributes' =>['language'=>$description->lang],
              '_cdata' => strip_tags($description->value)
            ]
          ];
        }
      }

      if(!is_null($estate->photos)){
        foreach($estate->photos as $photo){
          $photos[] = [
            '_attributes' =>['position'=>$photo->position],
            '_value' => $photo->path
          ];
        }
      }

      $extraFeatures['bedrooms'] = 0;
      $extraFeatures['bedrooms'] += (isset($rooms['camera-matrimoniale'])) ? $rooms['camera-matrimoniale'] : 0;
      $extraFeatures['bedrooms'] += (isset($rooms['camera-singola'])) ? $rooms['camera-singola'] : 0;
      if(isset($rooms['garage'])){
        $extraFeatures['garage'] = [
          '_attributes' => ['type'=>'PostoAuto'],
          '_value' => $rooms['garage']
        ];
      }
      $extraFeatures['heating'] = $riscaldamento->value;
      #$extraFeatures['garden'] = '';
      $extraFeatures['terrace'] = (isset($rooms['terrazza']) && $rooms['terrazza'] > 0) ? 'Y' : 'N' ;
      $extraFeatures['balcony'] = (isset($rooms['balcony']) && $rooms['balcone'] > 0) ? 'Y' : 'N' ;

      $extraFeatures['bathrooms'] = (isset($rooms['servizio']) && $rooms['servizio'] > 0) ? $rooms['servizio'] : 0 ;
      $extraFeatures['kitchen'] = (isset($rooms['cucina']) && $rooms['cucina'] > 0) ? 'Y' : 'N' ;

      $extraFeatures['elevator'] = (isset($binaryFields['ascensore'])) ? $binaryFields['ascensore'] : 'N' ;
      $extraFeatures['air-conditioning'] = (isset($binaryFields['aria-condizionata'])) ? $binaryFields['aria-condizionata'] : 'N' ;

      $extraFeatures['floor'] = $estate->piano;
      $extraFeatures['num-floor'] = $estate->PianiEdificio;

      $return['properties']['property'][] = [
        '_attributes' => ['operation'=>'write'],
        'unique-id' => $estate->ref,
        'date-updated' => date('Y-m-d',strtotime($estate->insert_timestamp)).'T'.date('H:i:s',strtotime($estate->insert_timestamp)),
        'date-expiration' => '',
        'transaction-type' => ($estate->Contratto == 'V') ? 'S' : 'R',
        'building'=>[
          '_attributes' => ['map'=>$buildingMap],
          'status' => $condizioniImmobile->value,
          'class' => $tipoCostruzione->value
        ],
        'agent' => [
          'office-name' => $estate->agency->agency_name,
          'email' => $estate->agency->agency_email
        ],
        'location' => [
          'country-code' => 'IT',
          'administrative-area' => $estate->administrative_area_level_1,
          'sub-administrative-area' => $estate->administrative_area_level_3,
          'city' => [
            '_attributes' => ['code' => $estate->ISTAT],
            '_value' => ($estate->locality != '') ? $estate->locality : $estate->administrative_area_level_3
          ],
          'locality' => [
            'thoroughfare' => [
              '_attributes' => ['display'=>'no'],
              '_value' => $estate->route.', '.$estate->street_number,
            ],
            'postal-code' => $estate->postal_code,
            'latitude' => $estate->lat,
            'longitude' => $estate->lng
          ]
        ],
        'features' => [
          'rooms' => $estate->NrLocali,
          'size' => [
            '_attributes' =>['unit'=>'m2'],
            '_value' => (string)$estate->MQSuperficie
          ],
          'price' => [
            '_attributes' => ['currency' => 'EUR','reserved'=>'no'],
            '_value' => (string)$estate->Prezzo
          ],
          'descriptions'=>$estateDescriptions,
          'energy-class' => $classeEnergetica->value,
          'energy-performance' => [
            '_attributes' => ['certified' => ($estate->InAttesaCertificazioneEnergetica == 1) ? 'no' : 'yes' ,'unit'=>$ipeUnitaMisura->value],
            '_value' => $estate->ClasseEnergeticaTxt
          ]
        ],
        'extra-features'=>$extraFeatures,
        'pictures' => ['picture-url'=>$pictures]
      ];
    }

    $result =  ArrayToXml::convert($return,[

      'rootElementName' => 'feed',
      '_attributes' => [
        'xmlns' => 'http://feed.immobiliare.it',
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemalocation' => 'http://feed.immobiliare.it/import/docs/xsd/v2.7.xsd',
      ],

    ]);
    header('Content-Type', 'text/xml');
    header('charset','utf-8');
    echo $result;
    exit;
  }
}
