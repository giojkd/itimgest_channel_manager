<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
  protected $table = 'immobiliareit_values';
  protected $primaryKey = 'unique_value_id';
}
