<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstateText extends Model
{
    protected $table = 'estate_txt';
}
